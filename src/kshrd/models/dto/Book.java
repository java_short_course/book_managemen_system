package kshrd.models.dto;

import java.sql.Date;
import java.util.List;


public class Book {

    private Integer id;
    private String title;
    private String description;
    private Author author;
    private Boolean status;
    private Date registerDate;
    private List<BookImage> bookImages;

    public Book() {
    }

    public Book(Integer id, String title, String description, Author author, Boolean status, Date registerDate, List<BookImage> bookImages) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.status = status;
        this.registerDate = registerDate;
        this.bookImages = bookImages;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public List<BookImage> getBookImages() {
        return bookImages;
    }

    public void setBookImages(List<BookImage> bookImages) {
        this.bookImages = bookImages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author=" + author +
                ", status=" + status +
                ", registerDate=" + registerDate +
                '}';
    }
}
