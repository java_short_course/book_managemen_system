package kshrd.models.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BMSConnection {

    public Connection connection = null;

    public void openConnection() {
        String url = "jdbc:postgresql://localhost:12345/j2se_book_management_system_db";
        try {
            connection = DriverManager.getConnection(url, "j2se_user", "12345");
            System.out.println("connection open");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            connection.close();
            System.out.println("connection close");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



}
