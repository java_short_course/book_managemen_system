package kshrd.models.dao;

import kshrd.models.dto.ChangePassword;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ChangePasswordDAO {

    private BMSConnection bmsConnection = new BMSConnection();
    private PreparedStatement preparedStatement;


    public Boolean passwordChanging(ChangePassword changePassword) {
        boolean isPasswordChanged = false;
        try {
            bmsConnection.openConnection();
            String sql = "SELECT password FROM bms_users " +
                    "WHERE id = ? AND password = md5(?) AND status is true";
            preparedStatement = bmsConnection.connection.prepareStatement(sql);
            preparedStatement.setInt(1, changePassword.getUserId());
            preparedStatement.setString(2, changePassword.getCurrentPassword());
            ResultSet currentPasswordResultSet = preparedStatement.executeQuery();
            if (currentPasswordResultSet.next()) {
                String setNewPasswordSQL = "UPDATE bms_users " +
                        "SET password = md5(?) " +
                        "WHERE id = ?;";
                preparedStatement = bmsConnection.connection.prepareStatement(setNewPasswordSQL);
                preparedStatement.setString(1, changePassword.getNewPassword());
                preparedStatement.setInt(2, changePassword.getUserId());
                int numberOfRowChanging = preparedStatement.executeUpdate();
                if (numberOfRowChanging > 0) {
                    isPasswordChanged = true;
                }
            } else {
                System.out.println("Current password not correct");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bmsConnection.closeConnection();
        }

        return isPasswordChanged;
    }


    public Boolean adminResetPassword(ChangePassword changePassword) {
        boolean isPasswordReseted = false;
        try {
            bmsConnection.openConnection();

            String resetPassword = "UPDATE bms_users " +
                    "SET password = md5(?) " +
                    "WHERE id = ?;";
            preparedStatement = bmsConnection.connection.prepareStatement(resetPassword);
            preparedStatement.setString(1, changePassword.getNewPassword());
            preparedStatement.setInt(2, changePassword.getUserId());
            int numberOfRowChanging = preparedStatement.executeUpdate();
            if (numberOfRowChanging > 0) {
                isPasswordReseted = true;
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bmsConnection.closeConnection();
        }

        return isPasswordReseted;
    }


}
