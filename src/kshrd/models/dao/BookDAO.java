package kshrd.models.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kshrd.models.dto.Author;
import kshrd.models.dto.Book;
import kshrd.models.dto.BookImage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BookDAO {

    private BMSConnection bmsConnection = new BMSConnection();
    private PreparedStatement preparedStatement;

    public List<String> authorListName() {
        List<String> authors = new ArrayList<>();
        bmsConnection.openConnection();
        String sql = "SELECT CONCAT(first_name, ' ' ,last_name) FROM bms_authors\n" +
                "WHERE status IS TRUE;";
        try {
            preparedStatement = bmsConnection.connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                authors.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        bmsConnection.closeConnection();
        return authors;
    }


    public Boolean insertBook(Book book) {
        boolean isInserted = false;
        bmsConnection.openConnection();

        String sql = "INSERT INTO bms_books (title, description, author_id) " +
                "VALUES (?, ?, (SELECT id from bms_authors " +
                "WHERE concat(first_name, ' ', last_name) LIKE ? AND status is TRUE ))";
        try {
            preparedStatement = bmsConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getDescription());
            preparedStatement.setString(3, book.getAuthor().getFullName());
            int numberOfRowInserted = preparedStatement.executeUpdate();
            if (numberOfRowInserted > 0)
                isInserted = true;

            //TODO: Get id of book that we just inserted and then insert to book_image
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                Integer bookId = resultSet.getInt(1);
                String sqlInsertBookImage = "INSERT INTO bms_book_images (book_id, image_url) \n" +
                        "VALUES (?, ?);";
                List<BookImage> bookImages = book.getBookImages();

                for (BookImage bookImage : bookImages) {
                    preparedStatement = bmsConnection.connection.prepareStatement(sqlInsertBookImage);
                    preparedStatement.setInt(1, bookId);
                    preparedStatement.setString(2, bookImage.getImageUrl());
                    preparedStatement.executeUpdate();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        bmsConnection.closeConnection();

        return isInserted;
    }

    public ObservableList<Book> bookObservableList() {
        ObservableList<Book> bookList = FXCollections.observableArrayList();
        bmsConnection.openConnection();
        String sql = "SELECT book.id, book.title, book.description, concat(authors.first_name, ' ', authors.last_name)\n" +
                "FROM bms_books book\n" +
                "JOIN bms_authors authors ON book.author_id = authors.id\n" +
                "WHERE book.status IS TRUE\n" +
                "ORDER BY book.id";
        try {
            preparedStatement = bmsConnection.connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setDescription(resultSet.getString(3));
                Author author = new Author();
                author.setFullName(resultSet.getString(4));
                book.setAuthor(author);

                bookList.add(book);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        bmsConnection.closeConnection();
        return bookList;
    }




}
