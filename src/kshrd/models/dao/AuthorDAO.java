package kshrd.models.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kshrd.models.dto.Author;

import java.sql.*;

public class AuthorDAO {

    private BMSConnection bmsConnection = new BMSConnection();
    private PreparedStatement preparedStatement;


    public ObservableList<Author> authorsList() {
        ObservableList<Author> authors = FXCollections.observableArrayList();

        try {
            bmsConnection.openConnection();
            String sql = "SELECT * FROM bms_authors where status IS TRUE ORDER  BY id";
            preparedStatement = bmsConnection.connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            Author author;
            while (resultSet.next()) {
                author = new Author();
                author.setId(resultSet.getInt(1));
                author.setFirstName(resultSet.getString(2));
                author.setLastName(resultSet.getString(3));
                author.setGender(resultSet.getString(4).charAt(0));
                author.setBirthDate(resultSet.getDate(5));
                author.setEmail(resultSet.getString(6));
                author.setPhoneNumber(resultSet.getString(7));
                author.setAddress(resultSet.getString(8));
                author.setImageUrl(resultSet.getString(9));

                // add object author to authors
                authors.add(author);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
           bmsConnection.closeConnection();
        }

        return  authors;
    }

    public Boolean insertAuthor(Author author) {
        boolean isInserted = false;
        try {
            bmsConnection.openConnection();
            String sql = "INSERT INTO bms_authors (first_name, last_name, gender, birth_date, email, phone_number, address, image_url) \n" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            preparedStatement = bmsConnection.connection.prepareStatement(sql);

            preparedStatement.setString(1, author.getFirstName());
            preparedStatement.setString(2, author.getLastName());
            preparedStatement.setString(3, author.getGender().toString());
            preparedStatement.setDate(4, author.getBirthDate());
            preparedStatement.setString(5, author.getEmail());
            preparedStatement.setString(6, author.getPhoneNumber());
            preparedStatement.setString(7, author.getAddress());
            preparedStatement.setString(8, author.getImageUrl());

            // execute
            int numberOfRowEffected = preparedStatement.executeUpdate();
            if (numberOfRowEffected > 0)
                isInserted = true;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bmsConnection.closeConnection();
        }

        return isInserted;
    }

    public Boolean updateAuthor (Author author) {
        boolean isUpdated = false;
        bmsConnection.openConnection();
        String sql = "UPDATE bms_authors SET " +
                "first_name = ?, last_name = ?, gender = ?, birth_date = ?, " +
                "email = ?, phone_number = ?, address = ? , image_url = ? " +
                "WHERE id = ?";
        try {
            PreparedStatement preparedStatement = bmsConnection.connection.prepareStatement(sql);
            preparedStatement.setString(1, author.getFirstName());
            preparedStatement.setString(2, author.getLastName());
            preparedStatement.setString(3, author.getGender().toString());
            preparedStatement.setDate(4, author.getBirthDate());
            preparedStatement.setString(5, author.getEmail());
            preparedStatement.setString(6, author.getPhoneNumber());
            preparedStatement.setString(7, author.getAddress());
            preparedStatement.setString(8, author.getImageUrl());
            preparedStatement.setInt(9, author.getId());

            int numberOfUpdatedRow = preparedStatement.executeUpdate();
            if (numberOfUpdatedRow > 0)
                isUpdated = true;

        } catch (SQLException e) {
            e.printStackTrace();
        }


        bmsConnection.closeConnection();

        return isUpdated;
    }


    public Boolean deleteAuthor(Author author) {
        Boolean isDeleted = false;
        bmsConnection.openConnection();
        String sql = "UPDATE bms_authors SET status = false WHERE id = ?";
        try {
            preparedStatement = bmsConnection.connection.prepareStatement(sql);
            preparedStatement.setInt(1, author.getId());
            int numberOfDeletedRow = preparedStatement.executeUpdate();
            if (numberOfDeletedRow > 0) {
                isDeleted = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        bmsConnection.closeConnection();
        return isDeleted;
    }

}
