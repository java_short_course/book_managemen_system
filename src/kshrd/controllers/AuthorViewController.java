package kshrd.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import kshrd.models.dao.AuthorDAO;
import kshrd.models.dto.Author;

import java.io.*;
import java.net.URL;
import java.sql.Date;
import java.util.*;

public class AuthorViewController implements Initializable {

    // ==== Control Variables ===

    @FXML
    private DatePicker birthDateField;

    @FXML
    private TextField phoneNumberField;

    @FXML
    private TextField emailField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField firstNameField;

    @FXML
    private RadioButton femaleRadioButton;

    @FXML
    private ImageView authorImageView;

    @FXML
    private Button editButton;

    @FXML
    private RadioButton maleRadioButton;

    @FXML
    private Button newButton;

    @FXML
    private Button saveButton;

    @FXML
    private TextField addressField;

    @FXML Button deleteButton;

    @FXML
    private Button uploadButton;

    @FXML
    private TableView<Author> authorTableView;

    @FXML
    private TableColumn<Author, Integer> idCol;

    @FXML
    private TableColumn<Author, String> firstNameCol;

    @FXML
    private TableColumn<Author, String> lastNameCol;

    @FXML
    private TableColumn<Author, Character> genderCol;

    @FXML
    private TableColumn<Author, Date> birthDateCol;

    @FXML
    private TableColumn<Author, String> emailCol;

    @FXML
    private TableColumn<Author, String> phoneNumberCol;

    @FXML
    private TableColumn<Author, String> addressCol;


    // ==== Normal Variables ===
    private ToggleGroup genderToggle = new ToggleGroup();
    private AuthorDAO authorDAO = new AuthorDAO();
    private File imageProfile = null;
    private Author selectedAuthor = new Author();
    List<Author>  selectedAuthors = new ArrayList<>();
    private Boolean saveStatus = true;
    Permission permission = new Permission();

    @FXML
    void uploadButtonPressed() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter =
                new FileChooser.ExtensionFilter("Image: ", "*.png", "*.jpg", "*.gif", "*.jpeg");
        fileChooser.getExtensionFilters().add(filter);
        imageProfile = fileChooser.showOpenDialog(null);
        if (imageProfile != null) {
            Image image = new Image(imageProfile.toURI().toString());
            authorImageView.setImage(image);
            System.out.println(imageProfile);
        }
    }

    @FXML
    void saveButtonPressed() {
        Author author = new Author();

        // TODO: Write image
        if (imageProfile != null) {
            try {

                String oldImageProfileName = imageProfile.getName();
                String imageExtension = oldImageProfileName.substring(oldImageProfileName.lastIndexOf("."));
                String newImageProfileName = UUID.randomUUID().toString() + imageExtension;
                String fileLocation = "/Users/temchannat/opt/Book_Management_System/images/";
                File path = new File(fileLocation);
                if (!path.exists()) {
                    path.mkdirs();
                }

                FileOutputStream fileOutputStream =
                        new FileOutputStream(fileLocation + newImageProfileName);
                // check if save or update
                if (saveStatus) {
                    author.setImageUrl(fileLocation + newImageProfileName);
                } else  {
                    selectedAuthor.setImageUrl(fileLocation + newImageProfileName);
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            FileInputStream fileInputStream =
                                    new FileInputStream(imageProfile);
                            int c;
                            while ((c = fileInputStream.read()) != -1) {
                                fileOutputStream.write(c);
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        if (saveStatus == true) {
            // TODO: Save new Author....
            //System.out.println("SAVE NEW AUTHOR....");

            author.setFirstName(firstNameField.getText().trim());
            author.setLastName(lastNameField.getText().trim());
            Character gender = 'M';
            if (femaleRadioButton.isSelected())
                gender = 'F';
            author.setGender(gender);
            author.setBirthDate(Date.valueOf(birthDateField.getValue()));
            author.setEmail(emailField.getText().trim());
            author.setPhoneNumber(phoneNumberField.getText().trim());
            author.setAddress(addressField.getText().trim());
            //System.out.println(author);

            if (authorDAO.insertAuthor(author)) {
                System.out.println("Inserted...");
                authorTableView.setItems(authorDAO.authorsList());
            } else {
                System.out.println("Insert failed...");
            }
        } else {
            // TODO: Update Author when saveStatus == false
            //System.out.println("Update AUTHOR....");
            selectedAuthor.setFirstName(firstNameField.getText().trim());
            selectedAuthor.setLastName(lastNameField.getText().trim());
            Character gender = 'M';
            if (femaleRadioButton.isSelected())
                gender = 'F';
            selectedAuthor.setGender(gender);
            selectedAuthor.setBirthDate(Date.valueOf(birthDateField.getValue()));
            selectedAuthor.setEmail(emailField.getText().trim());
            selectedAuthor.setPhoneNumber(phoneNumberField.getText().trim());
            selectedAuthor.setAddress(addressField.getText().trim());
            authorDAO.updateAuthor(selectedAuthor);
            authorTableView.setItems(authorDAO.authorsList());
        }

        newButton.setDisable(false);
        saveButton.setDisable(true);
        editButton.setDisable(true);
        uploadButton.setDisable(true);
        clearFields();
        setFieldDisable(true);


    }

    private void clearFields() {
        firstNameField.clear();
        lastNameField.clear();
        maleRadioButton.setSelected(true);
        birthDateField.setValue(null);
        emailField.clear();
        phoneNumberField.clear();
        addressField.clear();

        File file = new File ("/Users/temchannat/Documents/Java FX/Book Management System/src/kshrd/views/images/user.png");
        Image image = new Image(file.toURI().toString());
        authorImageView.setImage(image);

    }

    @FXML
    void deleteButtonPressed() {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText("Are you sure to delete?");
        alert.setTitle("Delete");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            selectedAuthors.forEach( author -> {
                authorDAO.deleteAuthor(author);
            });
            authorTableView.setItems(authorDAO.authorsList());
        }


    }

    @FXML
    void newButtonPressed() {
        saveStatus = true;
        setFieldDisable(false);
        firstNameField.clear();
        lastNameField.clear();
        firstNameField.requestFocus();
        newButton.setDisable(true);
        uploadButton.setDisable(false);
        saveButton.setDisable(false);
    }


    @FXML
    void editButtonPressed() {
        saveStatus = false;
        setFieldDisable(false);
        editButton.setDisable(true);
        saveButton.setDisable(false);

    }


    // TODO: get object of author when we click on row of tableView
    @FXML
    void getSelectedValueFromTableClick() {
        //System.out.println("clicked....");
        selectedAuthors = authorTableView.getSelectionModel().getSelectedItems();
         //get selected row from table
       // selectedAuthor = authorTableView.getSelectionModel().getSelectedItem();
        selectedAuthor = selectedAuthors.get(0);
        firstNameField.setText(selectedAuthor.getFirstName());
        lastNameField.setText(selectedAuthor.getLastName());
        if (selectedAuthor.getGender() == 'M')
            maleRadioButton.setSelected(true);
        else
            femaleRadioButton.setSelected(true);
        birthDateField.setValue(selectedAuthor.getBirthDate().toLocalDate());
        emailField.setText(selectedAuthor.getEmail());
        phoneNumberField.setText(selectedAuthor.getPhoneNumber());
        addressField.setText(selectedAuthor.getAddress());

        // Set image to authorImageView
        String oldImageUrl = selectedAuthor.getImageUrl();
        // check if oldImageUrl is null or empty
        if (oldImageUrl == null || oldImageUrl == "")
            oldImageUrl = "/Users/temchannat/Documents/Java FX/Book Management System/src/kshrd/views/images/user.png";

        File file = new File(oldImageUrl);

        // check if image is not found.
        if (!file.exists())
            file = new File("/Users/temchannat/Documents/Java FX/Book Management System/src/kshrd/views/images/user.png");

        Image image = new Image(file.toURI().toString());
        authorImageView.setImage(image);

        // check if user select more than 1 field
        if (selectedAuthors.size() > 1) {
            editButton.setDisable(true);
        } else {
            if (permission.hasRoleAdmin()) {
                permission.allowOnButtons(editButton);
            } else {
                permission.notAllowOnButtons(editButton);
            }
        }
        newButton.setDisable(true);

        //deleteButton.setDisable(false);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Add gender to group
        maleRadioButton.setToggleGroup(genderToggle);
        femaleRadioButton.setToggleGroup(genderToggle);
        maleRadioButton.setSelected(true);


        // Map between columns and it own class fields
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        genderCol.setCellValueFactory(new PropertyValueFactory<>("gender"));
        birthDateCol.setCellValueFactory(new PropertyValueFactory<>("birthDate"));
        emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));
        phoneNumberCol.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));

        authorTableView.setItems(authorDAO.authorsList());
        setFieldDisable(true);
        uploadButton.setDisable(true);
        editButton.setDisable(true);
        saveButton.setDisable(true);
        deleteButton.setDisable(true);

        // allow multi select of tableview
        authorTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);



        if (!permission.hasRoleAdmin()) {
            permission.notAllowOnButtons(newButton);
        }


    }

    private void setFieldDisable(Boolean isDisabled) {
        firstNameField.setDisable(isDisabled);
        lastNameField.setDisable(isDisabled);
        maleRadioButton.setDisable(isDisabled);
        femaleRadioButton.setDisable(isDisabled);
        birthDateField.setDisable(isDisabled);
        emailField.setDisable(isDisabled);
        phoneNumberField.setDisable(isDisabled);
        addressField.setDisable(isDisabled);
    }

}
