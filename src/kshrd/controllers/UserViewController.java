package kshrd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import kshrd.models.dao.ChangePasswordDAO;
import kshrd.models.dao.UserDAO;
import kshrd.models.dto.ChangePassword;
import kshrd.models.dto.User;

import java.net.URL;
import java.util.ResourceBundle;

public class UserViewController implements Initializable {

    @FXML
    private TableView<User> userTableView;

    @FXML
    private TableColumn<User, String> usernameColumn;

    @FXML private ComboBox<String> roleCombobox;

    @FXML
    private JFXTextField usernameField;

    @FXML
    private JFXButton changePasswordButton;

    @FXML
    private JFXButton deleteButton;

    @FXML
    private Label incorrectLabel;

    @FXML
    private JFXButton newButton;

    @FXML
    private JFXPasswordField passwordField;

    @FXML
    private JFXButton saveButton;

    @FXML
    private JFXPasswordField confirmPasswordField;

    @FXML
    private JFXPasswordField currentPasswordField;
    @FXML
    private JFXPasswordField newPasswordField;
    @FXML
    private JFXPasswordField confirmNewPasswordField;

    @FXML
    private Label incorrectChangePasswordLabel;

    private ChangePasswordDAO changePasswordDAO = new ChangePasswordDAO();
    User selectedUser = new User();

    @FXML
    void newButtonPressed() {

    }

    @FXML
    void deleteButtonPressed() {

    }

    private boolean isAdminLogin = false;
    ChangePassword changePassword = new ChangePassword();

    @FXML
    void changePasswordButtonPressed() {
        if (isAdminLogin) {
            // admin change password
            //System.out.println("admin change password");
            if (!newPasswordField.getText().trim().equals(confirmNewPasswordField.getText().trim())) {
                incorrectChangePasswordLabel.setVisible(true);
                confirmNewPasswordField.requestFocus();
            } else {
                changePassword.setNewPassword(newPasswordField.getText().trim());
                changePassword.setUserId(selectedUser.getId());
                boolean isPasswordReseted = changePasswordDAO.adminResetPassword(changePassword);
                System.out.println("Is password Reset : " + isPasswordReseted);
            }

        } else {
            // normal user chanage password
            //System.out.println("normal user change password");

            changePassword.setCurrentPassword(currentPasswordField.getText().trim());
            changePassword.setNewPassword(newPasswordField.getText().trim());
            changePassword.setUserId(UserDAO.userLogedin.getId());
            if (!newPasswordField.getText().trim().equals(confirmNewPasswordField.getText().trim())) {
                incorrectChangePasswordLabel.setVisible(true);
                confirmNewPasswordField.requestFocus();
            } else {
                incorrectChangePasswordLabel.setVisible(false);
                System.out.println(changePasswordDAO.passwordChanging(changePassword));
            }
        }

    }


    @FXML
    void saveButtonPressed() {

    }


    @FXML
    private void getUserTableViewClicked () {
        selectedUser = userTableView.getSelectionModel().getSelectedItem();
        changePasswordButton.setText("Reset Password");
        permission.setComponentVisibility(true, newPasswordField, confirmNewPasswordField);

    }


    private UserDAO userDAO = new UserDAO();
    private Permission permission = new Permission();





    @Override
    public void initialize(URL location, ResourceBundle resources) {

        usernameColumn.setCellValueFactory(new PropertyValueFactory<>("username"));

        userTableView.setItems(userDAO.userList());
        roleCombobox.getItems().addAll(userDAO.roleList());
        setFieldDisable(true);


        if (permission.hasRoleAdmin()) {
            isAdminLogin = true;
            permission.setComponentVisibility(false, newPasswordField, currentPasswordField, confirmNewPasswordField, incorrectChangePasswordLabel);
            permission.setComponentVisibility(true, userTableView, usernameField, passwordField, confirmPasswordField, roleCombobox, incorrectLabel, newButton, deleteButton, saveButton);
        } else {
            permission.setComponentVisibility(false,userTableView, usernameField, passwordField, confirmPasswordField, roleCombobox, incorrectLabel, newButton, deleteButton, saveButton, incorrectChangePasswordLabel);
        }


    }

    private void setFieldDisable(Boolean status) {
        usernameField.setDisable(status);
        passwordField.setDisable(status);
        confirmPasswordField.setDisable(status);
        roleCombobox.setDisable(status);


    }


}
