package kshrd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import kshrd.models.dao.UserDAO;
import kshrd.models.dto.Role;

import java.util.ArrayList;
import java.util.List;

public class Permission {


   public boolean hasRoleAdmin() {
       boolean isAdmin = false;
       UserDAO userDAO = new UserDAO();
       List<Role> roleList = userDAO.userLogedin.getRoles();;
       for (Role role : roleList) {
           // Check if roleName == "role_admin"
           if (role.getName().equalsIgnoreCase("role_admin")) {
               isAdmin = true;
           }
       }
       return isAdmin;
   }

    public boolean hasRoleUser() {
        boolean isUser = false;
        UserDAO userDAO = new UserDAO();
        List<Role> roleList = userDAO.userLogedin.getRoles();;
        for (Role role : roleList) {
            // Check if roleName == "role_user"
            if (role.getName().equalsIgnoreCase("role_user")) {
                isUser = true;
            }
        }
        return isUser;
    }

    public boolean hasRoleAuthor() {
        boolean isAuthor = false;
        UserDAO userDAO = new UserDAO();
        List<Role> roleList = userDAO.userLogedin.getRoles();;
        for (Role role : roleList) {
            // Check if roleName == "role_author"
            if (role.getName().equalsIgnoreCase("role_author")) {
                isAuthor = true;
            }
        }
        return isAuthor;
    }


   public void notAllowOnButtons(Object ...buttons) {
       for (Object button : buttons) {
           if (button instanceof Button) {
               ((Button) button).setDisable(true);
           }

           if (button instanceof JFXButton) {
               ((JFXButton) button).setDisable(true);
           }
       }
   }


   public void setComponentVisibility(Boolean status, Object ...objects) {
       for (Object object : objects ) {
           if (object instanceof TextField) {
               ((TextField) object).setVisible(status);
           }

           if (object instanceof JFXTextField) {
               ((JFXTextField) object).setVisible(status);
           }

           if (object instanceof TableView) {
               ((TableView) object).setVisible(status);
           }

           if (object instanceof JFXButton) {
               ((JFXButton) object).setVisible(status);
           }

           if (object instanceof Button) {
               ((Button) object).setVisible(status);
           }

           if (object instanceof JFXComboBox) {
               ((JFXComboBox) object).setVisible(status);
           }

           if (object instanceof JFXPasswordField) {
               ((JFXPasswordField) object).setVisible(status);
           }

           if (object instanceof Label) {
               ((Label) object).setVisible(status);
           }
       }
   }





    public void allowOnButtons(Object ...buttons) {
        for (Object button : buttons) {
            if (button instanceof Button) {
                ((Button) button).setDisable(false);
            }

            if (button instanceof JFXButton) {
                ((JFXButton) button).setDisable(false);
            }

        }
    }


}
